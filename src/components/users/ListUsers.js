import React, { useState, useEffect } from 'react';
import axios from 'axios'

export const ListUsers = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res => {
                console.log(res)
                setUsers(res.data)
            })
            .catch(err => alert(err))
    }, []);
    return (
        // <ul>
        //     {users.map(user => <li key={user.id}>{user.email}</li>)}
        // </ul>
        <div className='table-responsive'>
            <table className="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Website</th>
                        {/* <th scope="col">Handle</th> */}
                    </tr>
                </thead>
                <tbody>
                    {users.map(user => <tr>
                        <th scope="row">{user.id}</th>
                        <td>{user.name}</td>
                        <td>{user.username}</td>
                        <td>{user.email}</td>
                        <td>
                            <a href={"https://" + user.website}>{user.website}</a>
                        </td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    )
}
