import { ListUsers } from '../components/users/ListUsers';

export const Contact = () => {

    return (
        <main className="container">
            <div className="row">
                <div className="col mt-5">
                    <div className="bg-light p-5 rounded">
                        <h1>Contact Page</h1>
                        <ListUsers />
                    </div>
                </div>
            </div>
        </main>
    )
}
