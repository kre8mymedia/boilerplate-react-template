import React from 'react'

export const Home = () => {
    return (
        <main className="container">
            <div className="row">
                <div className="col mt-5">
                    <div className="bg-light p-5 rounded">
                        <h1>Home Page</h1>
                        <p
                            className="lead"
                        >
                            This example is a quick exercise to illustrate how fixed to top navbar works. As you scroll, it will remain fixed to the top of your browser’s viewport.
                        </p>
                    </div>
                </div>
            </div>
        </main>
    )
}
